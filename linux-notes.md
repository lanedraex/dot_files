# Manjaro/Linux tips

- [Manjaro/Linux tips](#manjarolinux-tips)
  - [Swap Caps Lock / Escape](#swap-caps-lock--escape)
    - [VS Code](#vs-code)
  - [Invisible widgets in KDE](#invisible-widgets-in-kde)
  - [Installing Discord](#installing-discord)
    - [Prepare to build Discord](#prepare-to-build-discord)
    - [Build using Pamac](#build-using-pamac)
  - [VSCode SpellRight Extension (hunspell setup)](#vscode-spellright-extension-hunspell-setup)
  - [Civilization 5 crash on launch](#civilization-5-crash-on-launch)

## Swap Caps Lock / Escape

Use command `setxkbmap -option caps:escape`.

### VS Code

https://github.com/Microsoft/vscode/issues/23991

`"keyboard.dispatch": "keyCode"`

## Invisible widgets in KDE

https://lists.debian.org/debian-kde/2015/09/msg00124.html

Edit file `$home/.config/plasma-org.kde.plasma.desktop-appletsrc` and search for the widget
to remove:

```bash
[Containments][45][Applets][47]
immutability=1
plugin=org.kde.plasma.systemmonitor.cpu
```

## Installing Discord

https://forum.manjaro.org/t/how-to-install-discord/22224

### Prepare to build Discord

> Import the GPG key needed for building llvm, libcxx and libcxxabi

`gpg --recv-keys A2C794A986419D8A`

> You need some tools to be able to build

`sudo pacman -Syu --needed base-devel git`

### Build using Pamac

> Install Pamac if needed (most KDE and LXQt except beta)

`sudo pacman -Syu pamac`

> Build Discord

`pamac build discord`

## VSCode SpellRight Extension (hunspell setup)

The `Dictionaries` folder might not exist, also pay attention if you're using the `OSS` VScode.

`ln -s /usr/share/hunspell ~/.config/Code\ -\ OSS/Dictionaries`

## Civilization 5 crash on launch

https://forum.manjaro.org/t/civilization-v-crashes-on-launch/54739/5

https://bbs.archlinux.org/viewtopic.php?id=238288

Set launch options to:

`LD_PRELOAD=/usr/lib32/libopenal.so.1 %command%`