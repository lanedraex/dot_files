" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Keybindings ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
" Buffer manipulation keys:
" Del ; :bd ; close current buffer
" PageDown ; :vertical split ; split current buffer vertically
" End ; :on ; close all splits

" Buffer navigation keys:
" Alt + ArrowUp ; :wincmd w ; switch between buffers
" ArrowUp ; :wincmd k ; switch to buffer Up
" ArrowDown ; :wincmd j ; switch to buffer Down
" ArrowLeft ; :wincmd h ; switch to buffer left
" ArrowRight ; :wincmd l ; switch to buffer right

" Function keys used:
" F3 ; :Neoformat ; auto-format file
" F9 ; listing and selecting a buffer
" F10 ; :Files ; call fzf list files
" F11 ; clear search buffer
" F12 ; remove trailing whitespace
" ==========================================================================

" ----------------------------- BUNDLE ------------------------------------
" Plugin Manager: vim-plug
" Specify a directory for plugins (for Neovim: ~/.local/share/nvim/plugged)
call plug#begin('$nvim_plugins')

" Make sure you use single quotes

" Deoplete is the abbreviation of "dark powered neo-completion".
" It provides an asynchronous keyword completion system in the current buffer.
" Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
" Plug 'sebastianmarkow/deoplete-rust'

" Nvim-Completion-Manager is a fast, extensible, async completion framework.
Plug 'roxma/nvim-completion-manager'
" Javascript completion.
" Plug 'roxma/nvim-cm-tern', { 'do': 'npm install' }
" Language server protocol framework.
Plug 'autozimu/LanguageClient-neovim', { 'do': ':UpdateRemotePlugins' }

" This is a Vim plugin that provides Rust file detection, syntax highlighting, formatting, Syntastic integration, and more.
Plug 'rust-lang/rust.vim'

" This plugin allows vim to use Racer for Rust code completion and navigation.
Plug 'racer-rust/vim-racer'

" A (Neo)vim plugin for formatting code.
Plug 'sbdchd/neoformat'

" fzf is a general-purpose command-line fuzzy finder.
Plug 'junegunn/fzf', { 'dir': '$fzf_dir', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" A light and configurable statusline/tabline for Vim
Plug 'itchyny/lightline.vim'

" Insert or delete brackets, parens, quotes in pair.
Plug 'https://github.com/jiangmiao/auto-pairs.git'

" Syntastic is a syntax checking plugin for Vim created by Martin Grenfell.
Plug 'https://github.com/vim-syntastic/syntastic.git'

" Vim plugin that uses clang for completing C/C++ code.
Plug 'https://github.com/Rip-Rip/clang_complete.git'

" This is a clang completer for deoplete.
"Plug 'https://github.com/tweekmonster/deoplete-clang2.git'

" Vim plugin for better Common Lisp integration (slime).
Plug 'https://github.com/kovisoft/slimv.git'

" Vlime is a Common Lisp dev environment for Vim (and Neovim), similar to
" SLIME.
" Plug 'https://github.com/l04m33/vlime.git'

" +++++++++++++++++++++++++++++ Color schemes ++++++++++++++++++++++++++++++
Plug 'https://github.com/mhartington/oceanic-next.git'
Plug 'https://github.com/chriskempson/base16-vim.git'
Plug 'https://github.com/fmoralesc/molokayo.git'
Plug 'https://github.com/morhetz/gruvbox.git'
Plug 'https://github.com/rakr/vim-one.git'
Plug 'https://github.com/jacoborus/tender.vim.git'
" ==========================================================================

" +++++++++++++++++++++++++++++ Better Language Support  ++++++++++++++++++++++++++++++
Plug 'https://github.com/sheerun/vim-polyglot.git'
Plug 'https://github.com/NLKNguyen/vim-lisp-syntax.git'
Plug 'https://github.com/beyondmarc/hlsl.vim.git'
" ==========================================================================

" Initialize plugin system
call plug#end()
" ===============================================================================

" *************************************************************************
" Editor
set tabstop=8
set softtabstop=4
set shiftwidth=4
" round indent to nearest shiftwidth multiple
set shiftround
" uses spaces instead of tabs
set expandtab
set encoding=utf-8
set fileencodings=utf-8
set autoread
set relativenumber
set hidden
" Show the current row and column at the bottom right.
set ruler
set updatetime=300 " set updatetime to shorter value
set lazyredraw " avoid scrolling slow down by buffering screen updates

if !has("gui_running")
    " Mintty supports control sequences for changing cursor style.
    " Block cursor in normal mode, line cursor in insert mode.
    let &t_ti.="\e[1 q"
    let &t_SI.="\e[5 q"
    let &t_EI.="\e[1 q"
    let &t_te.="\e[0 q"
    " Avoiding [Escape] timeout issues in Vim.
    let &t_ti.="\e[?7727h"
    let &t_te.="\e[?7727l"
    noremap <Esc>0[ <Esc>
    noremap! <Esc>0[ <Esc>
endif

if (has("nvim"))
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif

if (has("termguicolors"))
    set termguicolors
endif

set background=dark
colorscheme gruvbox

" Turn off sounds.
set visualbell t_vb=
au GuiEnter * set visualbell t_vb=

" Remove all trailing whitespace by pressing F12
nnoremap <F12> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>

" Clear search buffer (clears the last search highlights)
nnoremap <F11> :let @/=""<CR>

" A numbered list of file names is printed, type a single number and press
" enter to select file.
nnoremap <F9> :buffers<CR>:buffer<Space>
nnoremap <Del> :bd<CR>

" Move between splits by using arrow keys, switch between splits by holding
" ALT.
nnoremap <silent> <A-Up> :wincmd w<CR>
nnoremap <silent> <Up> :wincmd k<CR>
nnoremap <silent> <Down> :wincmd j<CR>
nnoremap <silent> <Left> :wincmd h<CR>
nnoremap <silent> <Right> :wincmd l<CR>
" Split buffer vertically :vs
nnoremap <silent> <PageDown> :vertical split<CR>
" Closes all splits except the one active.
nnoremap <silent> <End> :on<CR>

" Highlight keywords in comments like TODO, FIXME, WARNING, NOTE, REGION, ENDREGION
augroup highlight_keyword
    autocmd!
    autocmd WinEnter,VimEnter * :silent! call matchadd('Todo', 'TODO\|FIXME\|WARNING\|NOTE\|Plugin:\|REGION\|ENDREGION', -1)
augroup END

" For Windows Users to back to temp directory
set backup
set backupdir=$wdev/temp
set backupskip=$wdev/temp/*
set directory=$wdev/temp
set writebackup

" Automatically closing the scratch window at the top of the vim window on finishing a
" complete or leaving insert.
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

" deoplete tab-complete
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
inoremap <expr><s-tab> pumvisible() ? "\<c-p>" : "\<s-tab>"
" ===============================================================================

" *************************************************************************
" Plugin: deoplete
" Use deoplete.
let g:deoplete#enable_at_startup = 1
" ===============================================================================

" *************************************************************************
" Plugin: deoplete-rust
" Set fully qualified path to racer binary. If it is in your PATH already use which racer. (required)
let g:deoplete#sources#rust#racer_binary='$cargo_bin/racer'

" Set Rust source code path (when cloning from Github usually ending on /src). (required)
let g:deoplete#sources#rust#rust_source_path='$rust_src'

" To disable default key mappings (gd & K) add the following
" let g:deoplete#sources#rust#disable_keymap=1

" Set max height of documentation split.
" let g:deoplete#sources#rust#documentation_max_height=20

" ===============================================================================

" *************************************************************************
" Plugin: nvim-completion-manager
" Supress annoying completion messages.
set shortmess+=c

" Fuzzy search.
" let g:cm_matcher = { 'module': 'cm_matchers.abbrev_matcher' }
" ===============================================================================

" *************************************************************************
" Plugin: vim-racer
let g:racer_cmd = $cargo_bin
" ===============================================================================

" *************************************************************************
" Plugin: neoformat
noremap <F3> :Neoformat<CR>
" Configure enabled formatters.
let g:neoformat_enabled_python = ['rustfmt']
" Enable alignment
let g:neoformat_basic_format_align = 1
" Enable tab to spaces conversion
let g:neoformat_basic_format_retab = 1
" Enable trimmming of trailing whitespace
let g:neoformat_basic_format_trim = 1
" Debug
"let g:neoformat_verbose = 1

" ===============================================================================

" *************************************************************************
" Plugin: fzf
" Invoke :Files command from fzf.
nnoremap <F10> :Files<CR>

" This is the default extra key bindings
let g:fzf_action = {
            \ 'ctrl-t': 'tab split',
            \ 'ctrl-x': 'split',
            \ 'ctrl-v': 'vsplit' }

" Default fzf layout
" - down / up / left / right
let g:fzf_layout = { 'down': '~40%' }

" Customize fzf colors to match your color scheme
let g:fzf_colors =
            \ { 'fg':      ['fg', 'Normal'],
            \ 'bg':      ['bg', 'Normal'],
            \ 'hl':      ['fg', 'Comment'],
            \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
            \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
            \ 'hl+':     ['fg', 'Statement'],
            \ 'info':    ['fg', 'PreProc'],
            \ 'prompt':  ['fg', 'Conditional'],
            \ 'pointer': ['fg', 'Exception'],
            \ 'marker':  ['fg', 'Keyword'],
            \ 'spinner': ['fg', 'Label'],
            \ 'header':  ['fg', 'Comment'] }

" Enable per-command history.
" CTRL-N and CTRL-P will be automatically bound to next-history and
" previous-history instead of down and up. If you don't like the change,
" explicitly bind the keys to down and up in your $FZF_DEFAULT_OPTS.
let g:fzf_history_dir = $fzf_history

" Mapping selecting mappings
nmap <leader><tab> <plug>(fzf-maps-n)
xmap <leader><tab> <plug>(fzf-maps-x)
omap <leader><tab> <plug>(fzf-maps-o)

" Insert mode completion
imap <c-x><c-k> <plug>(fzf-complete-word)
imap <c-x><c-f> <plug>(fzf-complete-path)
imap <c-x><c-j> <plug>(fzf-complete-file-ag)
imap <c-x><c-l> <plug>(fzf-complete-line)

" Advanced customization using autoload functions
inoremap <expr> <c-x><c-k> fzf#vim#complete#word({'left': '15%'})
" ===============================================================================

" *************************************************************************
" Plugin: Lightline
let g:lightline = {
            \ 'colorscheme': 'gruvbox',
            \ 'active': {
            \    'left': [['mode', 'paste'],
            \               ['readonly', 'relativepath', 'modified']]
            \ },
            \ 'component_function': {
            \ 'filename': 'LightLineFilename'
            \ }
            \ }
function! LightLineFilename()
    return expand('%')
endfunction
" ===============================================================================

" *************************************************************************
" Plugin: Auto-Pairs
" Fixes auto-pairs inserting <SNR>33_AutoPairsReturn when working with a C++
" file.
let g:AutoPairsMapCR = 0
imap <silent><CR> <CR><Plug>AutoPairsReturn
" ===============================================================================

" *************************************************************************
" Plugin: Syntastic
let g:syntastic_mode_map = { 'mode': 'passive' }
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_cpp_compiler = 'clang++'
let g:syntastic_cpp_compiler_options = ' -std=c++1z -stdlib=libc++'

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
" ===============================================================================

" *************************************************************************
" Plugin: Clang Complete
" Set the clang_library_path to the directory containing file named libclang.{dll,so,dylib}.
let g:clang_library_path = $clang_lib
" ===============================================================================

" *************************************************************************
" Plugin: Deoplete Clang
"let g:deoplete#sources#clang#executable = $clang_exe
"let g:deoplete#sources#clang#flags = ['-I$wdev/libs/EASTL/include', '-I$dx12_shared', '-I$dx12_um']
"let g:deoplete#sources#clang#std = { 'cpp': 'c++14' }
" ===============================================================================


" *************************************************************************
" Plugin: SLIMV
" start swank server
let g:slimv_swank_cmd = '!screen -d -m -t REPL-SBCL sbcl --load $nvim_plugins/slimv/slime/start-swank.lisp'

let g:slimv_preferred = 'sbcl'

" alow quick keybindings for functions like \ed
let g:slimv_keybindings = 2

" let g:slimv_leader = '\'

" vertical split right 4
let g:slimv_repl_split = 4

" ===============================================================================

" *************************************************************************
" Plugin: Vlime
" nnoremap <silent> <F8> :!sbcl --load $nvim_plugins/vlime/lisp/start-vlime.lisp<CR>
" ===============================================================================

" *************************************************************************
" Plugin: Vim-Polyglot
" vim-cpp-enhanced-highlight
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_class_decl_highlight = 1
let g:cpp_experimental_template_highlight = 1

" ===============================================================================
