# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/home/lanedraex/.cargo/bin:/home/lanedraex/.rustup:/home/lanedraex/neovim/bin:$cmake_dir/bin:$cquery_dir/bin:$LD_LIBRARY_PATH:

export LD_LIBRARY_PATH="$HOME"/.multirust/toolchains/nightly-x86_64-unknown-linux-gnu/lib

shopt -s cdable_vars
export wdev="$HOME"/../../mnt/c/dev
export winc="$HOME"/../../mnt/c
export nvim_plugins="$HOME"/.local/share/nvim/plugged
export cargo_bin="$HOME"/.cargo/bin
export rust_src="$HOME"/.multirust/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src
export fzf_dir="$HOME"/.fzf
export fzf_history="$HOME"/.local/share/fzf-history
export clang_lib="$HOME"/../../usr/lib/llvm-5.0/lib/libclang-5.0.so.1
export clang_exe="$HOME"/../../usr/lib/llvm-5.0/bin
export slime_dot="$HOME"/.slime
export program_files="$HOME"/../../mnt/c/Program\ Files\ \(x86\)
export dx12_shared="$program_files"/Windows\ Kits/10/Include/10.0.15063.0/shared
export dx12_um="$program_files"/Windows\ Kits/10/Include/10.0.15063.0/um
export qt_include="$winc"/tools/Qt/5.9.1/msvc2015_64/include
export neovim_dir="$HOME"/neovim
export cmake_dir="$HOME"/download/cmake-3.11.3
export unreal_dir="$winc"/tools/unreal_engine/UE_4.19
export cquery_dir="$HOME"/download/cquery/build/release
export tools="$HOME"/tools

export scripts="$HOME"/download/scripts

alias nvim="$HOME"/neovim/bin/nvim
alias cquery="$cquery_dir"/bin/cquery
alias ag=rg
alias esp_rs="$tools"/esp-rs/build.sh

# ++++++++++++++++++++++++++ Config: fzf ++++++++++++++++++++++++++++++++++++++
# Use `ripgrep` instead of the default find command for listing path candidates.
# - The first argument to the function is the base path to start traversal.
# - `rg` only lists files, so we use `with-dir` script to augment the output.
_fzf_compgen_path() {
    rg --files "$1" | with-dir "$1"
}

# Use `rg` to generate the list for directory completion.
_fzf_compgen_dir() {
    rg --files "$1" | only-dir "$1"
}

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# -----------------------------------------------------------------------------
