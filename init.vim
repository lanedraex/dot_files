" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Keybindings ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
" Buffer manipulation keys:
" Del ; :bd ; close current buffer
" PageDown ; :vertical split ; split current buffer vertically
" End ; :on ; close all splits

" Buffer navigation keys:
" Alt + ArrowUp ; :wincmd w ; switch between buffers
" ArrowUp ; :wincmd k ; switch to buffer Up
" ArrowDown ; :wincmd j ; switch to buffer Down
" ArrowLeft ; :wincmd h ; switch to buffer left
" ArrowRight ; :wincmd l ; switch to buffer right

" Function keys used:
" F3 ; :Neoformat ; auto-format file
" F9 ; listing and selecting a buffer
" F10 or <leader>ff ; :Files ; call fzf list files
" F11 ; clear search buffer
" F12 ; remove trailing whitespace

" <leader> commands `\`:
" <leader>fs ; :w ; save current file
" ==========================================================================

" ----------------------------- BUNDLE ------------------------------------
" Plugin Manager: vim-plug
" Specify a directory for plugins (for Neovim: ~/.local/share/nvim/plugged)
call plug#begin('$nvim_plugins')

" Make sure you use single quotes

" Turned off on: 08/19/2018 11:01 PM
" Deoplete is the abbreviation of "dark powered neo-completion".
" It provides an asynchronous keyword completion system in the current buffer.
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
" C/C++ Completion for deoplete using clang. 2018/06/01 05:41PM
" Plug 'https://github.com/Shougo/deoplete-clangx.git'
" Plug 'sebastianmarkow/deoplete-rust'
" Include completion framework for neocomplete/deoplete. : 01/31/2018 12:00AM
" Plug 'Shougo/neoinclude.vim'

" Turned off on: 01/30/2018 11:54 PM
" Nvim-Completion-Manager is a fast, extensible, async completion framework.
"Plug 'roxma/nvim-completion-manager'
" Javascript completion.
" Plug 'roxma/nvim-cm-tern', { 'do': 'npm install' }
" Language server protocol framework.
"Plug 'autozimu/LanguageClient-neovim', { 'do': ':UpdateRemotePlugins' }
" ncm-clang is a clang completion integration for nvim-completion-manager
"Plug 'roxma/ncm-clang'
" ncm-clang is a racer completion integration for nvim-completion-manager
"Plug 'roxma/nvim-cm-racer'

" This is a Vim plugin that provides Rust file detection, syntax highlighting, formatting, Syntastic integration, and more.
Plug 'rust-lang/rust.vim'

" 2018/10/13 06:48AM
" This plugin allows vim to use Racer for Rust code completion and navigation.
" Plug 'racer-rust/vim-racer'

" A (Neo)vim plugin for formatting code.
Plug 'sbdchd/neoformat'

" fzf is a general-purpose command-line fuzzy finder.
Plug 'junegunn/fzf', { 'dir': '$fzf_dir', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" A light and configurable statusline/tabline for Vim
Plug 'itchyny/lightline.vim'
Plug 'itchyny/vim-gitbranch'
" Provides ALE indicator for lightline. 2018/10/13 08:11 AM
" Plug 'maximbaz/lightline-ale.vim'

" Insert or delete brackets, parens, quotes in pair.
Plug 'https://github.com/jiangmiao/auto-pairs.git'

" Syntastic is a syntax checking plugin for Vim created by Martin Grenfell.
Plug 'https://github.com/vim-syntastic/syntastic.git'

" Vim plugin that uses clang for completing C/C++ code. 2018/05/27 05:25PM
" Plug 'https://github.com/Rip-Rip/clang_complete.git'

" This is a clang completer for deoplete.
"Plug 'https://github.com/tweekmonster/deoplete-clang2.git'

" Vim plugin for better Common Lisp integration (slime).
Plug 'https://github.com/kovisoft/slimv.git'

" Useful Vim commands to close buffers. 2018/05/19 04:02AM
Plug 'https://github.com/Asheq/close-buffers.vim.git'

" Vlime is a Common Lisp dev environment for Vim (and Neovim), similar to
" SLIME.
" Plug 'https://github.com/l04m33/vlime.git'

" LSP support for vim and neovim. 2018/05/20 08:00AM
Plug 'autozimu/LanguageClient-neovim', {
            \'branch': 'next',
            \'do': 'bash install.sh'
            \}

" 2018/10/13 06:02AM
" ALE (Asynchronous Lint Engine) is a plugin for providing linting in NeoVim 0.2.0+ and
" Vim 8 while you edit your text files, and acts as a Vim Language Server Protocol client.
" Plug 'w0rp/ale'

" Async LSP plugin for vim8 and neovim. 2018/10/13 07:42 AM
" Plug 'prabirshrestha/async.vim'
" Plug 'prabirshrestha/vim-lsp'
" Plug 'prabirshrestha/asyncomplete.vim'
" Plug 'prabirshrestha/asyncomplete-lsp.vim'

" Add devicons for vim.
Plug 'ryanoasis/vim-devicons'


" +++++++++++++++++++++++++++++ Color schemes ++++++++++++++++++++++++++++++
Plug 'https://github.com/mhartington/oceanic-next.git'
Plug 'https://github.com/chriskempson/base16-vim.git'
Plug 'https://github.com/fmoralesc/molokayo.git'
Plug 'https://github.com/morhetz/gruvbox.git'
Plug 'https://github.com/rakr/vim-one.git'
Plug 'https://github.com/jacoborus/tender.vim.git'
Plug 'https://github.com/rafi/awesome-vim-colorschemes.git' "2018/06/06 06:43AM
Plug 'nightsense/vimspectr' "2018/06/06 06:43AM
Plug 'chriskempson/vim-tomorrow-theme' "2018/06/06 07:51AM
" ==========================================================================

" +++++++++++++++++++++++++++++ Better Language Support  ++++++++++++++++++++++++++++++
Plug 'https://github.com/sheerun/vim-polyglot.git'
Plug 'https://github.com/NLKNguyen/vim-lisp-syntax.git'
Plug 'https://github.com/beyondmarc/hlsl.vim.git'
"Plug 'octol/vim-cpp-enhanced-highlight', { 'for': ['c', 'cpp'] }
" ==========================================================================

" Initialize plugin system
call plug#end()
" ===============================================================================

" *************************************************************************
" Editor
let mapleader="\<Space>"
set tabstop=8
set softtabstop=4
set shiftwidth=4
" round indent to nearest shiftwidth multiple
set shiftround
" uses spaces instead of tabs
set expandtab
set encoding=utf-8
set fileencodings=utf-8
set autoread
set relativenumber
set hidden
" Show the current row and column at the bottom right.
set ruler
set updatetime=300 " set updatetime to shorter value
set lazyredraw " avoid scrolling slow down by buffering screen updates
set inccommand=nosplit "live update while using :substitute
set clipboard+=unnamedplus " use clipboard instead of + * registers

if !has("gui_running")
    " Mintty supports control sequences for changing cursor style.
    " Block cursor in normal mode, line cursor in insert mode.
    let &t_ti.="\e[1 q"
    let &t_SI.="\e[5 q"
    let &t_EI.="\e[1 q"
    let &t_te.="\e[0 q"
    " Avoiding [Escape] timeout issues in Vim.
    let &t_ti.="\e[?7727h"
    let &t_te.="\e[?7727l"
    noremap <Esc>0[ <Esc>
    noremap! <Esc>0[ <Esc>
endif

if (has("nvim"))
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif

" This if is closing Neovim on mintty whenever I double click (or click
" multiple times) on the screen with the left mouse button. Be it in normal
" mode, or insert mode.
" This happens even if I put this command at the end of my vimrc.
" has("termguicolors") returns 1 and it works properly if I set it manually,
" but it starts quitting again on mouse click if I enable it via vimrc.
"! if (has("termguicolors"))
"!     set termguicolors
"! endif

" PaperColor theme config
let g:PaperColor_Theme_Options = {
            \ 'theme': {
            \   'default': {
            \       'transparent_background': 1
            \   }
            \ }
            \ }
set background=dark
colorscheme jellybeans

" Turn off sounds.
set visualbell t_vb=
au GuiEnter * set visualbell t_vb=

" Remove all trailing whitespace by pressing F12
nnoremap <F12> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>

" Clear search buffer (clears the last search highlights)
nnoremap <F11> :let @/=""<CR>

" A numbered list of file names is printed, type a single number and press
" enter to select file.
nnoremap <F9> :buffers<CR>:buffer<Space>
nnoremap <Del> :bd<CR>

" Move between splits by using arrow keys, switch between splits by holding
" ALT.
nnoremap <silent> <A-Up> :wincmd w<CR>
nnoremap <silent> <Up> :wincmd k<CR>
nnoremap <silent> <Down> :wincmd j<CR>
nnoremap <silent> <Left> :wincmd h<CR>
nnoremap <silent> <Right> :wincmd l<CR>
" Split buffer vertically :vs
nnoremap <silent> <PageDown> :vertical split<CR>
" Closes all splits except the one active.
nnoremap <silent> <End> :on<CR>

" Highlight keywords in comments like TODO, FIXME, WARNING, NOTE, REGION, ENDREGION
augroup highlight_keyword
    autocmd!
    autocmd WinEnter,VimEnter * :silent! call matchadd('Todo', 'TODO\|FIXME\|WARNING\|NOTE\|Plugin:\|REGION\|ENDREGION', -1)
augroup END

" For Windows Users to back to temp directory
set backup
set backupdir=$wdev/temp
set backupskip=$wdev/temp/*
set directory=$wdev/temp
set writebackup

" Automatically closing the scratch window at the top of the vim window on finishing a
" complete or leaving insert.
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

" deoplete tab-complete
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
inoremap <expr><s-tab> pumvisible() ? "\<c-p>" : "\<s-tab>"
" ===============================================================================

" *************************************************************************
" Plugin: deoplete
" Use deoplete.
let g:deoplete#enable_at_startup = 1
let g:deoplete#enable_smart_case = 1

let g:deoplete#sources = {}
let g:deoplete#sources.cpp = ['LanguageClient']
let g:deoplete#sources.c = ['LanguageClient']
let g:deoplete#sources.rust = ['LanguageClient']
let g:deoplete#sources.vim = ['vim']
" ===============================================================================

" *************************************************************************
" Plugin: neoinclude
"let g:neoinclude#paths = '$wdev/libs/boost_1_66_0/boost'
" ===============================================================================

" *************************************************************************
" Plugin: deoplete-rust
" Set fully qualified path to racer binary. If it is in your PATH already use which racer. (required)
let g:deoplete#sources#rust#racer_binary='$cargo_bin/racer'

" Set Rust source code path (when cloning from Github usually ending on /src). (required)
let g:deoplete#sources#rust#rust_source_path='$rust_src'

" To disable default key mappings (gd & K) add the following
" let g:deoplete#sources#rust#disable_keymap=1

" Set max height of documentation split.
" let g:deoplete#sources#rust#documentation_max_height=20

" ===============================================================================

" *************************************************************************
" Plugin: nvim-completion-manager
" Supress annoying completion messages.
set shortmess+=c

" Fuzzy search.
" let g:cm_matcher = { 'module': 'cm_matchers.abbrev_matcher' }
" ===============================================================================

" *************************************************************************
" Plugin: ncm-clang
" default key mapping is annoying
"let g:clang_make_default_keymappings = 0

" ncm-clang is auto detecting compile_commands.json and .clang_complete file
"let g:clang_auto_user_options = ''

"func WrapClangGoTo()
"    let cwd = getcwd()
"    let info = ncm_clang#compilation_info()
"    exec 'cd ' . info['directory']
"    try
"        let b:clang_user_options = join(info['args'], ' ')
"        call g:ClangGoToDeclaration()
"    catch
"    endtry
" restore
"    exec 'cd ' . cwd
"endfunc

" map to `gd` key
"autocmd FileType c,cpp nnoremap <buffer> gd :call WrapClangGoTo()<CR>

" work with `w0rp/ale`
"let g:ale_linters = {
"            \ 'cpp': ['clang'],
"            \}
"autocmd BufEnter *.cpp,*.h,*.hpp,*.hxx let g:ale_cpp_clang_options = join(ncm_clang#compilation_info()['args'], ' ')

" ===============================================================================

" *************************************************************************
" Plugin: vim-racer
let g:racer_cmd = $cargo_bin
" ===============================================================================

" *************************************************************************
" Plugin: neoformat
noremap <F3> :Neoformat<CR>
let g:neoformat_python_rustfmt = {
            \ 'exe': 'rustfmt',
            \ 'args': ['+nightly']
            \ }
" Configure enabled formatters.
let g:neoformat_enabled_python = ['rustfmt', 'clang_format']
" Enable alignment
let g:neoformat_basic_format_align = 1
" Enable tab to spaces conversion
let g:neoformat_basic_format_retab = 1
" Enable trimmming of trailing whitespace
let g:neoformat_basic_format_trim = 1
" Debug
" let g:neoformat_verbose = 1
" prevent undoing just the the formatting / skips over neoformat changes
" augroup fmt
"     autocmd!
"     autocmd BufWritePre * undojoin | Neoformat
" augroup END

" ===============================================================================

" *************************************************************************
" Plugin: fzf
" Invoke :Files command from fzf.
nnoremap <F10> :Files<CR>

" This is the default extra key bindings
let g:fzf_action = {
            \ 'ctrl-t': 'tab split',
            \ 'ctrl-x': 'split',
            \ 'ctrl-v': 'vsplit' }

" Default fzf layout
" - down / up / left / right
let g:fzf_layout = { 'down': '~40%' }

" Customize fzf colors to match your color scheme
let g:fzf_colors =
            \ { 'fg':      ['fg', 'Normal'],
            \ 'bg':      ['bg', 'Normal'],
            \ 'hl':      ['fg', 'Comment'],
            \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
            \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
            \ 'hl+':     ['fg', 'Statement'],
            \ 'info':    ['fg', 'PreProc'],
            \ 'prompt':  ['fg', 'Conditional'],
            \ 'pointer': ['fg', 'Exception'],
            \ 'marker':  ['fg', 'Keyword'],
            \ 'spinner': ['fg', 'Label'],
            \ 'header':  ['fg', 'Comment'] }

" Enable per-command history.
" CTRL-N and CTRL-P will be automatically bound to next-history and
" previous-history instead of down and up. If you don't like the change,
" explicitly bind the keys to down and up in your $FZF_DEFAULT_OPTS.
let g:fzf_history_dir = $fzf_history

" Mapping selecting mappings
nmap <leader><tab> <plug>(fzf-maps-n)
xmap <leader><tab> <plug>(fzf-maps-x)
omap <leader><tab> <plug>(fzf-maps-o)

" Insert mode completion
imap <c-x><c-k> <plug>(fzf-complete-word)
imap <c-x><c-f> <plug>(fzf-complete-path)
imap <c-x><c-j> <plug>(fzf-complete-file-ag)
imap <c-x><c-l> <plug>(fzf-complete-line)

" Advanced customization using autoload functions
inoremap <expr> <c-x><c-k> fzf#vim#complete#word({'left': '15%'})

" Ignore files specified by .gitignore.
let $FZF_DEFAULT_COMMAND = 'ag -g ""'
" ===============================================================================

" *************************************************************************
" Plugin: Lightline
let g:lightline = {
            \   'colorscheme': 'jellybeans',
            \   'active': {
            \       'left': [['mode', 'paste'],
            \           ['gitbranch', 'readonly', 'filename', 'relativepath', 'modified']],
            \       'right': [['lineinfo'], ['percent'],
            \           ['fileformat', 'filetype']]
            \   },
            \   'component': {
            \       'lsp': '0'
            \   }
            \ }
function! LightLineFilename()
    return expand('%')
endfunction
" ===============================================================================

" *************************************************************************
" Plugin: Auto-Pairs
" Fixes auto-pairs inserting <SNR>33_AutoPairsReturn when working with a C++
" file.
let g:AutoPairsMapCR = 0
imap <silent><CR> <CR><Plug>AutoPairsReturn
" ===============================================================================

" *************************************************************************
" Plugin: Syntastic
let g:syntastic_mode_map = { 'mode': 'passive' }
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_cpp_checkers = ["clang_check", "gcc"]
let g:syntastic_clang_check_config_file = "$wdev/dot_files/.syntastic_clang_check_config"


let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
" ===============================================================================

" *************************************************************************
" Plugin: Clang Complete
" Set the clang_library_path to the directory containing file named libclang.{dll,so,dylib}.
let g:clang_library_path = $clang_lib
" ===============================================================================

" *************************************************************************
" Plugin: Deoplete Clang
"let g:deoplete#sources#clang#executable = $clang_exe
"let g:deoplete#sources#clang#flags = ['-I$wdev/libs/EASTL/include', '-I$dx12_shared', '-I$dx12_um']
"let g:deoplete#sources#clang#std = { 'cpp': 'c++14' }
" ===============================================================================


" *************************************************************************
" Plugin: SLIMV
" start swank server
let g:slimv_swank_cmd = '!screen -d -m -t REPL-SBCL sbcl --load $nvim_plugins/slimv/slime/start-swank.lisp'

let g:slimv_preferred = 'sbcl'

" alow quick keybindings for functions like \ed
let g:slimv_keybindings = 2

" let g:slimv_leader = '\'

" vertical split right 4
let g:slimv_repl_split = 4

" ===============================================================================

" *************************************************************************
" Plugin: Vlime
" nnoremap <silent> <F8> :!sbcl --load $nvim_plugins/vlime/lisp/start-vlime.lisp<CR>
" ===============================================================================

" *************************************************************************
" Plugin: Vim-Polyglot
" vim-cpp-enhanced-highlight
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_class_decl_highlight = 1
let g:cpp_experimental_template_highlight = 1
let g:cpp_concepts_highlight = 1

" ===============================================================================

" *************************************************************************
" Plugin: LanguageClient-neovim
" required for operations modifying multiple buffers, like rename
set hidden
let g:LanguageClient_serverCommands = {
            \ 'rust': ['rustup', 'run', 'nightly', 'rls'],
            \ 'cpp': ['cquery', '--log-file=$wdev/temp/cquery/cq.log'],
            \ 'c': ['cquery', '--log-file=$wdev/temp/cquery/cq.log']
            \ }

let g:LanguageClient_loadSettings = 1
let g:LanguageClient_settingsPath= '$HOME/.config/nvim/settings.json'
"set completefunc=LanguageClient#complete
nnoremap <silent> gh :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> gn :call LanguageClient#textDocument_references()<CR>
nnoremap <silent> gr :call LanguageClient#textDocument_rename()<CR>
nnoremap <silent> ge :call LanguageClient#explainErrorAtPoint()<CR>

function PreviewWindowOpened()
    for nr in range(1, winnr('$'))
        if getwinvar(nr, "&pvw") == 1
            " found a preview
            return 1
        endif
    endfor
    return 0
endfun

set splitbelow
set splitright
map <silent> <Esc> :pclose<CR>

" autocmd CursorMoved && if PreviewWindowOpened() == 1 | pclose | endif
" ===============================================================================


" *************************************************************************
" Plugin: deoplete-clangx
" call deoplete#custom#var('clangx', 'clang_library', '$clang_lib')
" ===============================================================================

" *************************************************************************
" Plugin: ale
" enable completion where available.
let g:ale_completion_enabled = 1

" change sign used for warnings/errors
" let g:ale_sign_error = '>>'
" let g:ale_sign_warning = '--'
"
" display ale stuff in the status line
let g:airline#extensions#ale#enabled = 1
" ===============================================================================

" *************************************************************************
" Plugin: lightline-ale
" register the components
" let g:lightline = {}
" let g:lightline.component_expand = {
            \  'linter_checking': 'lightline#ale#checking',
            \  'linter_warnings': 'lightline#ale#warnings',
            \  'linter_errors': 'lightline#ale#errors',
            \  'linter_ok': 'lightline#ale#ok',
            \ }

" set color of components
" let g:lightline.component_type = {
            \     'linter_checking': 'left',
            \     'linter_warnings': 'warning',
            \     'linter_errors': 'error',
            \     'linter_ok': 'left',
            \ }

" add components to the lightline
" let g:lightline.active = { 'right': [[ 'linter_checking', 'linter_errors', 'linter_warnings', 'linter_ok' ]] }

" ===============================================================================

" *************************************************************************
" Plugin: vim-lsp
if executable('rls')
    au User lsp_setup call lsp#register_server({
                \ 'name': 'rls',
                \ 'cmd': {server_info->['rustup', 'run', 'nightly', 'rls']},
                \ 'whitelist': 'rust',
                \ })
endif

let g:lsp_signs_enabled = 1
let g:lsp_diagnostics_echo_cursor = 1

let g:ls_log_verbose = 1
let g:lsp_log_file = expand('$wdev/temp/vim-lsp.log')

" ===============================================================================

" *************************************************************************
" Plugin: asyncomplete
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<cr>"

let g:asyncomplete_remove_duplicates = 1
let g:asyncomplete_smart_completion = 1
let g:asyncomplete_auto_popup = 1

let g:asyncomplete_log_file = expand('$wdev/temp/asyncomplete.log')

autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif

" ===============================================================================


" *************************************************************************
" Editor: Keybindings

"set autowrite=1
" b -> Buffer management
" bd: kill buffer
nnoremap <leader>bd :bdelete<CR>
" bm: kill other buffers
nnoremap <leader>bm :CloseOtherBuffers<CR>
" bn: next buffer
nnoremap <leader>bn :bnext<CR>
" bp: previous buffer
nnoremap <leader>bp :bprev<CR>

" f -> File commands
" ff: find files
nnoremap <silent> <leader>ff :Files<CR>
" fs: save file
nnoremap <leader>fs :w<CR>
" fed: open init.vim
nnoremap <leader>fed :e $MYVIMRC<CR>

" m -> Major mode commands
noremap <leader>m= :Neoformat<CR>
" mm: lsp context menu
noremap <leader>mm :call LanguageClient_contextMenu()<CR>
" md: find definition
noremap <leader>md :call LanguageClient#textDocument_definition()<CR>
" mn: find references
noremap <leader>mn :call LanguageClient#textDocument_references()<CR>
" mr: refactor rename
noremap <leader>mr :call LanguageClient#textDocument_rename()<CR>
" mh: get information on hover
noremap <leader>mh :call LanguageClient#textDocument_hover()<CR>
" me: explain error at point
noremap <leader>me :call LanguageClient#explainErrorAtPoint()<CR>

" q -> Quit
" qs: Save file and quit nvim
nnoremap <leader>qs :wq<CR>
" qq: Tries to quit nvim
nnoremap <leader>qq :q<CR>
" qQ: Quit without saving
nnoremap <leader>qQ :q!<CR>
" qr: reload init.vim
nnoremap <leader>qr :so $MYVIMRC<CR>

" s -> Search
" sc: Clear search buffer (clears the last search highlights)
nnoremap <leader>sc :let @/=""<CR>

" w -> Window management
" w-: Split buffer horizontally :vs
nnoremap <silent> <leader>w- :horizontal split<CR>
" w/: Split buffer vertically :vs
nnoremap <silent> <leader>w/ :vertical split<CR>
" wD: Closes all splits except the one active.
nnoremap <leader>wD :on<CR>

" Other
" frw: Remove all trailing whitespace
nnoremap <leader>frw :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>

" ===============================================================================
