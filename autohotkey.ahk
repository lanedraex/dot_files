; Author: fwompner gmail com
; Author: lanedraex hotmail com

; Editors
keystate = ""

is_inside_code_editor() {
    classname = ""
    WinGetClass, classname, A
    return classname = "Vim" or classname = "Emacs" or classname = "Qt5QWindowIcon" or classname = "PuTTY" or classname = "VirtualConsoleClass" or classname = "mintty" or classname = "ConsoleWindowClass" or WinActive("Visual Studio")
}

#InstallKeybdHook
SetCapsLockState, alwaysoff
Capslock::
SetTitleMatchMode, RegEx
if is_inside_code_editor() {
    Send {LControl Down}
    KeyWait, CapsLock
        Send {LControl Up}
    if ( A_PriorKey = "CapsLock" ) {
        Send {Esc}
    }
    return
} else {
    GetKeyState, keystate, CapsLock, T
        if (keystate = "D") {
            SetCapsLockState, Off
        } else {
            SetCapsLockState, On
        }
    return
}

; https://superuser.com/questions/425873/replace-with-and-with-using-autohotkey/425881#425881
#IF is_inside_code_editor()
*$[::
SetTitleMatchMode, RegEx
    if (GetKeyState("Shift")) {
        SendInput, {[ Down}
        return
    } else {
        SendInput, {{ Down}
        return
    }

*$]::
SetTitleMatchMode, RegEx
    if (GetKeyState("Shift")) {
        SendInput, {] Down}
        return
    } else {
        SendInput, {} Down}
        return
    }

*$[ Up::
SetTitleMatchMode, RegEx
    if (GetKeyState("Shift")) {
        SendInput, {[ Up}
        return
    } else {
        SendInput, {{ Up}
        return
    }

*$] Up::
SetTitleMatchMode, RegEx
    if (GetKeyState("Shift")) {
        SendInput, {] Up}
        return
    } else {
        SendInput, {} Up}
        return
    }

*$-::
SetTitleMatchMode, RegEx
    if (GetKeyState("Shift")) {
        SendInput, {- Down}
        return
    } else {
        SendInput, {_ Down}
        return
    }

*$- Up::
SetTitleMatchMode, RegEx
    if (GetKeyState("Shift")) {
        SendInput, {- Up}
        return
    } else {
        SendInput, {_ Up}
        return
    }
#IF

; Games

is_inside_arpg() {
    classname = ""
        WinGetClass, classname, A
        return classname = "Torchlight II" or WinActive("Torchlight II")
}

; Hold left mouse button. (Mouse forward button)
#IF is_inside_arpg()
XButton2::
    SendInput % "{Click " . ( GetKeyState("LButton") ? "Up}" : "Down}" )
return

; Click left mouse button multiple times. (Mouse back button)
XButton1::
    Loop {
        Sleep 5
        KeyWait, XButton1
        if error_level = 0
            break
        Click
    }
return
#IF

is_inside_magicka() {
    classname = ""
    WinGetClass, classname, A
    return classname = "Magicka" or WinActive("Magicka")
}

#IF is_inside_magicka()
    SetKeyDelay, 50, 50, Play
    ; Revive
    1::SendPlay wa
    ; Haste
    2::SendPlay asf
    ; Nullify
    3::SendPlay se
    ; ThunderBolt 
    4::SendPlay qfasa
    ; Conflagration 
    5::SendPlay fqffqffq
    ; Teleport 
    6::SendPlay asa
    ; ThunderStorm 
    7::SendPlay fqfqasa
    ; SummonDeath 
    8::SendPlay srrqrs
    ; MeteorShower 
    9::SendPlay fdfqdf
    ; Vortex 
    0::SendPlay rqsrqerq
return
#IF
